package id.the.movie.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import id.the.movie.room.AppDatabase
import id.the.movie.room.GenreDao
import id.the.movie.room.MovieDao
import id.the.movie.room.MovieDetailDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PersistenceModule {

  @Provides
  @Singleton
  fun provideRoomDataBase(@ApplicationContext context: Context): AppDatabase {
    return Room
      .databaseBuilder(context, AppDatabase::class.java, "TheMovies.db")
      .allowMainThreadQueries()
      .build()
  }

  @Provides
  @Singleton
  fun provideMovieDao(appDatabase: AppDatabase): MovieDao {
    return appDatabase.movieDao()
  }

  @Provides
  @Singleton
  fun provideGenreDao(appDatabase: AppDatabase): GenreDao {
    return appDatabase.genreDao()
  }

  @Provides
  @Singleton
  fun provideMovieDetailDao(appDatabase: AppDatabase): MovieDetailDao {
    return appDatabase.movieDetailDao()
  }

}
