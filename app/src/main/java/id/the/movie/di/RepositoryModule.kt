package id.the.movie.di
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import id.the.movie.repository.DiscoverRepository
import id.the.movie.repository.GenreRepository
import id.the.movie.repository.MovieRepository
import id.the.movie.room.GenreDao
import id.the.movie.room.MovieDao
import id.the.movie.room.MovieDetailDao
import id.the.movie.utils.api.service.MovieService
import id.the.movie.utils.api.service.TheDiscoverService

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

  @Provides
  @ViewModelScoped
  fun provideDiscoverRepository(
    discoverService: TheDiscoverService,
    movieDao: MovieDao,
  ): DiscoverRepository {
    return DiscoverRepository(discoverService, movieDao)
  }

  @Provides
  @ViewModelScoped
  fun provideMovieRepository(
    movieService: MovieService,
    movieDao: MovieDao,
    movieDetailDao : MovieDetailDao
  ): MovieRepository {
    return MovieRepository(movieService, movieDao,movieDetailDao)
  }

  @Provides
  @ViewModelScoped
  fun provideGenreRepository(
    movieService: MovieService,
    genreDao: GenreDao
  ): GenreRepository {
    return GenreRepository(movieService, genreDao)
  }

}
