package id.the.movie.models.network

import id.the.movie.models.Keyword
import id.the.movie.models.NetworkResponseModel

data class KeywordListResponse(
  val id: Int,
  val keywords: List<Keyword>
) : NetworkResponseModel
