package id.the.movie.models.entity

import android.os.Parcelable
import androidx.room.Entity
import id.the.movie.models.Keyword
import id.the.movie.models.Review
import id.the.movie.models.Video
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(primaryKeys = [("id")])
data class MovieDetail(
    val id: Int,
    val poster_path: String?,
    val backdrop_path: String?,
    val title: String,
    val vote_count: Int?,
    val vote_average: Float,
    val original_language: String? = "",
    val release_date: String?,
    val runtime: Int?,
    val overview: String?,
    var genres: List<Genre>?
) : Parcelable
