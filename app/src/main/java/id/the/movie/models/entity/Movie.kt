package id.the.movie.models.entity

import android.os.Parcelable
import androidx.room.Entity
import id.the.movie.models.Keyword
import id.the.movie.models.Review
import id.the.movie.models.Video
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(primaryKeys = [("id")])
data class Movie(
    var page: Int,
    var keywords: List<Keyword>? = ArrayList(),
    var videos: List<Video>? = ArrayList(),
    var reviews: List<Review>? = ArrayList(),
    var detail: List<MovieDetail>?,
    val poster_path: String?,
    val adult: Boolean,
    val overview: String,
    val release_date: String?,
    val genre_ids: List<Int>,
    val id: Int,
    val original_title: String,
    val original_language: String,
    val title: String,
    val backdrop_path: String?,
    val popularity: Float,
    val vote_count: Int,
    val runtime: Int,
    val video: Boolean,
    val vote_average: Float
) : Parcelable
