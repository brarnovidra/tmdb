package id.the.movie.models.network

import id.the.movie.models.NetworkResponseModel
import id.the.movie.models.Review

class ReviewListResponse(
  val id: Int,
  val page: Int,
  val results: List<Review>,
  val total_pages: Int,
  val total_results: Int
) : NetworkResponseModel
