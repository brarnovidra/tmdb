package id.the.movie.models.network

import id.the.movie.models.NetworkResponseModel
import id.the.movie.models.Video


data class VideoListResponse(
  val id: Int,
  val results: List<Video>
) : NetworkResponseModel
