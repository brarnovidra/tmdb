package id.the.movie.models.entity

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(primaryKeys = [("id")])
data class Genre(
    val id: Int,
    val name: String,
) : Parcelable
