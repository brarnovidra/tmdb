package id.the.movie.models.network

data class TheMovieErrorResponse(
  val code: Int,
  val message: String
)
