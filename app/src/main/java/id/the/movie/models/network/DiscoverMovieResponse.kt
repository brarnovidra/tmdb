package id.the.movie.models.network

import id.the.movie.models.NetworkResponseModel
import id.the.movie.models.entity.Movie


data class DiscoverMovieResponse(
  val page: Int,
  val results: List<Movie>,
  val total_results: Int,
  val total_pages: Int
) : NetworkResponseModel
