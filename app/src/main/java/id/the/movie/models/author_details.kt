package id.the.movie.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class author_details(
    val name: String?,
    val username: String?,
    val avatar_path: String?,
    val rating: Float
) : Parcelable
