package id.the.movie.models.network

import id.the.movie.models.NetworkResponseModel
import id.the.movie.models.entity.Genre

data class MovieResponse(
    var id: Int?,
    val poster_path: String?,
    val backdrop_path: String?,
    val title: String?,
    val vote_count: Int?,
    val vote_average: Float,
    val genre_ids: List<Int>?,
    val original_language: String? = "",
    val release_date: String?,
    val runtime: Int?,
    val overview: String?,
    var genres: List<Genre>?
): NetworkResponseModel