package id.the.movie.models.network

import id.the.movie.models.entity.Genre
import id.the.movie.models.NetworkResponseModel

data class GenreListResponse(
    val genres: List<Genre>
): NetworkResponseModel