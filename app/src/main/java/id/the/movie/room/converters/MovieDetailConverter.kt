package id.the.movie.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.the.movie.models.entity.Genre
import id.the.movie.models.entity.MovieDetail

open class MovieDetailConverter {
    @TypeConverter
    fun fromString(value: String): List<MovieDetail>? {
        val listType = object : TypeToken<List<MovieDetail>>() {}.type
        return Gson().fromJson<List<MovieDetail>>(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<MovieDetail>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}
