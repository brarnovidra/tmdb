package id.the.movie.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import id.the.movie.models.entity.Genre
import id.the.movie.models.entity.Movie
import id.the.movie.models.entity.MovieDetail
import id.the.movie.room.converters.*

@Database(
  entities = [(Movie::class),(Genre::class),(MovieDetail::class)],
  version = 3, exportSchema = false
)
@TypeConverters(
  value = [
    (StringListConverter::class), (IntegerListConverter::class),
    (KeywordListConverter::class), (VideoListConverter::class), (GenreListConverter::class),(MovieDetailConverter::class),
    (ReviewListConverter::class)
  ]
)
abstract class AppDatabase : RoomDatabase() {
  abstract fun movieDao(): MovieDao
  abstract fun genreDao(): GenreDao
  abstract fun movieDetailDao(): MovieDetailDao
}
