package id.the.movie.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import id.the.movie.models.entity.MovieDetail

@Dao
interface MovieDetailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieDetail(movieDetail: List<MovieDetail>)

    @Update
    fun updateMovieDetail(movieDetail: MovieDetail)

    @Query("SELECT * FROM movieDetail")
    fun getMovieDetail(): List<MovieDetail>

    @Query("SELECT * FROM movieDetail WHERE id = :id_")
    fun getMovieDetail(id_: Int): MovieDetail

}