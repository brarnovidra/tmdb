package id.the.movie.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import id.the.movie.models.entity.Genre

@Dao
interface GenreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGenre(genre: List<Genre>)

    @Update
    fun updateGenre(genre: Genre)

    @Query("SELECT * FROM Genre")
    fun getGenreList(): List<Genre>

    @Query("SELECT * FROM Genre WHERE id = :id_")
    fun getGenre(id_: Int): Genre

}