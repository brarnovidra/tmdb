package id.the.movie.utils.api.service

import com.skydoves.sandwich.ApiResponse
import id.the.movie.models.network.DiscoverMovieResponse

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheDiscoverService {

  @GET("/3/discover/movie?language=en&sort_by=popularity.desc")
  suspend fun fetchDiscoverMovie(@Query("page") page: Int): ApiResponse<DiscoverMovieResponse>

  @GET("/3/discover/movie")
  suspend fun fetchDiscoverMovieGenre(@Query("page") page: Int, @Query("with_genres") movie_genre : String): ApiResponse<DiscoverMovieResponse>

}
