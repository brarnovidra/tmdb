package id.the.movie.utils.api.service

import com.skydoves.sandwich.ApiResponse
import id.the.movie.models.entity.Movie
import id.the.movie.models.entity.MovieDetail
import id.the.movie.models.network.*
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieService {
    @GET("/3/movie/{movie_id}/keywords")
    suspend fun fetchKeywords(@Path("movie_id") id: Int): ApiResponse<KeywordListResponse>

    @GET("/3/movie/{movie_id}/videos")
    suspend fun fetchVideos(@Path("movie_id") id: Int): ApiResponse<VideoListResponse>

    @GET("/3/movie/{movie_id}/reviews")
    suspend fun fetchReviews(@Path("movie_id") id: Int): ApiResponse<ReviewListResponse>

    @GET("/3/genre/movie/list?language=en-US")
    suspend fun fetchGenre(): ApiResponse<GenreListResponse>

    @GET("/3/movie/{movie_id}")
    suspend fun fetchDetail(@Path("movie_id") id: Int): ApiResponse<MovieDetail>

}