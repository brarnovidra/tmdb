package id.the.movie

import android.app.Application
import com.facebook.stetho.Stetho
import com.skydoves.sandwich.SandwichInitializer
import dagger.hilt.android.HiltAndroidApp
import id.the.movie.utils.Network
import timber.log.Timber

@HiltAndroidApp
class MainActivity : Application() {

    override fun onCreate() {
        super.onCreate()
        SandwichInitializer.sandwichOperator = Network<Any>(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Stetho.initializeWithDefaults(this)
    }
}