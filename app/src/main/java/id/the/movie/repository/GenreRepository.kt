package id.the.movie.repository

import androidx.annotation.WorkerThread
import com.skydoves.sandwich.suspendOnSuccess
import id.the.movie.room.GenreDao
import id.the.movie.utils.api.service.MovieService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import timber.log.Timber

class GenreRepository constructor(
    private val movieService: MovieService,
    private val genreDao: GenreDao
) : Repository {

    init {
        Timber.d("Injection GenreRepository")
    }

    @WorkerThread
    fun loadGenres(page: Int, success: () -> Unit) = flow {
        var genre = genreDao.getGenreList()
        if (genre.isEmpty()) {
            val response = movieService.fetchGenre()
            response.suspendOnSuccess {
                genre = data.genres
                genreDao.insertGenre(genre)
                emit(genre)
            }
        } else {
            emit(genre)
        }
    }.onCompletion { success() }.flowOn(Dispatchers.IO)

}
