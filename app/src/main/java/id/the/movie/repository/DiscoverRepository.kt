package id.the.movie.repository

import androidx.annotation.WorkerThread
import com.skydoves.sandwich.suspendOnSuccess
import id.the.movie.activity.view.adapter.MovieListAdapter
import id.the.movie.room.MovieDao
import id.the.movie.utils.api.ApiConstanst
import id.the.movie.utils.api.service.TheDiscoverService

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import timber.log.Timber


class DiscoverRepository constructor(
    private val discoverService: TheDiscoverService,
    private val movieDao: MovieDao
) : Repository {

  init {
    Timber.d("Injection DiscoverRepository")
  }

  @WorkerThread
  fun loadMovies(page: Int, success: () -> Unit) = flow {
    var movies = movieDao.getMovieList(page)
    if (movies.isEmpty()) {
      val response = discoverService.fetchDiscoverMovie(page)
      response.suspendOnSuccess {
        movies = data.results
        movies.forEach { it.page = page }
        movieDao.insertMovieList(movies)
        emit(movies)
      }
    } else {
      emit(movies)
    }
  }.onCompletion { success() }.flowOn(Dispatchers.IO)

  @WorkerThread
  fun loadMoviesGenre(page: Int,genre : String, success: () -> Unit) = flow {
    var movies = movieDao.getMovieList(page)
    val response = discoverService.fetchDiscoverMovieGenre(page,genre)
    response.suspendOnSuccess {
      movies = data.results
      movies.forEach { it.page = page }
      movieDao.insertMovieList(movies)
      emit(movies)
    }
  }.onCompletion { success() }.flowOn(Dispatchers.IO)

}
