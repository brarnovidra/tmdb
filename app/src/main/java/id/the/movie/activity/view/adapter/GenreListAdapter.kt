package id.the.movie.activity.view.adapter

import android.content.Intent
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.bindables.binding
import id.the.movie.R
import id.the.movie.activity.view.ui.main.MainActivity
import id.the.movie.databinding.ItemGenreBinding
import id.the.movie.models.entity.Genre
import id.the.movie.utils.api.ApiConstanst

class GenreListAdapter : RecyclerView.Adapter<GenreListAdapter.GenreListViewHolder>() {

    private val items: MutableList<Genre> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreListViewHolder{
        val binding = parent.binding<ItemGenreBinding>(R.layout.item_genre)
        return GenreListViewHolder(binding).apply {
            binding.root.setOnClickListener {
                val movie = adapterPosition.takeIf { it != RecyclerView.NO_POSITION }
                    ?: return@setOnClickListener
                Toast.makeText( it.context,"Genre " + items[movie].name, Toast.LENGTH_SHORT).show()
                ApiConstanst.GENRES = items[movie].id.toString()
                ApiConstanst.itemsMovie = true
                val intent = Intent (it.context, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                it.context.startActivity(intent)
            }
        }
    }

    override fun onBindViewHolder(holder: GenreListViewHolder, position: Int) {
        with(holder.binding) {
            genre = items[position]
            executePendingBindings()
        }
    }

    override fun getItemCount(): Int = items.size

    fun addGenreList(genre: List<Genre>) {
        items.addAll(genre)
        notifyItemRangeInserted(items.size + 1, genre.size)
    }

    class GenreListViewHolder(val binding: ItemGenreBinding) :
        RecyclerView.ViewHolder(binding.root)
}
