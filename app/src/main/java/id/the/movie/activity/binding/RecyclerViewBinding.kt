package id.the.movie.activity.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.skydoves.baserecyclerviewadapter.BaseAdapter
import com.skydoves.baserecyclerviewadapter.RecyclerViewPaginator

import com.skydoves.whatif.whatIfNotNull
import com.skydoves.whatif.whatIfNotNullAs
import com.skydoves.whatif.whatIfNotNullOrEmpty
import id.the.movie.R
import id.the.movie.activity.view.adapter.GenreListAdapter
import id.the.movie.activity.view.adapter.MovieListAdapter
import id.the.movie.activity.view.adapter.ReviewListAdapter
import id.the.movie.activity.view.adapter.VideoListAdapter
import id.the.movie.activity.view.ui.main.MainActivityViewModel
import id.the.movie.extension.visible
import id.the.movie.models.Keyword
import id.the.movie.models.Review
import id.the.movie.models.Video
import id.the.movie.models.entity.Genre
import id.the.movie.models.entity.Movie
import id.the.movie.utils.api.ApiConstanst

object RecyclerViewBinding {

  @JvmStatic
  @BindingAdapter("adapter")
  fun bindAdapter(view: RecyclerView, baseAdapter: BaseAdapter) {
    view.adapter = baseAdapter
  }

  @JvmStatic
  @BindingAdapter("adapterMovieList")
  fun bindAdapterMovieList(view: RecyclerView, movies: List<Movie>?) {
    movies.whatIfNotNull {
      (view.adapter as? MovieListAdapter)?.addMovieList(it)
    }
  }

  @JvmStatic
  @BindingAdapter("paginationMovieList")
  fun paginationMovieList(view: RecyclerView, viewModel: MainActivityViewModel) {
    RecyclerViewPaginator(
      recyclerView = view,
      isLoading = { viewModel.isMovieListLoading },
      loadMore = { viewModel.postMoviePage(it) },
      onLast = { false }
    ).run {
      threshold = 4
      currentPage = 1
    }
  }

  @JvmStatic
  @BindingAdapter("adapterVideoList")
  fun bindAdapterVideoList(view: RecyclerView, videos: List<Video>?) {
    videos.whatIfNotNullOrEmpty { items ->
      view.adapter.whatIfNotNullAs<VideoListAdapter> {
        it.addVideoList(items)
        view.visible()
      }
    }
  }

  @JvmStatic
  @BindingAdapter("adapterReviewList")
  fun bindAdapterReviewList(view: RecyclerView, reviews: List<Review>?) {
    view.setHasFixedSize(true)
    reviews.whatIfNotNullOrEmpty { items ->
      view.adapter.whatIfNotNullAs<ReviewListAdapter> {
        it.addReviewList(items)
        view.visible()
      }
    }
  }

  @JvmStatic
  @BindingAdapter("mapKeywordList")
  fun bindMapKeywordList(chipGroup: ChipGroup, keywords: List<Keyword>?) {
    keywords.whatIfNotNullOrEmpty {
      chipGroup.visible()
      for (keyword in it) {
        chipGroup.addView(
          Chip(chipGroup.context).apply {
            text = keyword.name
            isCheckable = false
            setTextAppearanceResource(R.style.ChipTextStyle)
            setChipBackgroundColorResource(R.color.colorPrimary)
          }
        )
      }
    }
  }

  @JvmStatic
  @BindingAdapter("adapterGenreList")
  fun bindAdapterGenreList(view: RecyclerView, genre: List<Genre>?) {
    genre.whatIfNotNullOrEmpty { items ->
      view.adapter.whatIfNotNullAs<GenreListAdapter> {
        it.addGenreList(items)
        view.visible()
      }
    }
  }

}
