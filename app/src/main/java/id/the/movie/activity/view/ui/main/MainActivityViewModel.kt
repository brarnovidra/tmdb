package id.the.movie.activity.view.ui.main

import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.skydoves.bindables.BindingViewModel
import com.skydoves.bindables.asBindingProperty
import com.skydoves.bindables.bindingProperty
import dagger.hilt.android.lifecycle.HiltViewModel
import id.the.movie.models.entity.Genre
import id.the.movie.models.entity.Movie
import id.the.movie.repository.DiscoverRepository
import id.the.movie.repository.GenreRepository
import id.the.movie.utils.api.ApiConstanst

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
  private val discoverRepository: DiscoverRepository,
  private val genreRepository: GenreRepository,
) : BindingViewModel() {

  @get:Bindable
  var isMovieListLoading: Boolean by bindingProperty(false)
    private set

  @get:Bindable
  var isGenreLoading: Boolean by bindingProperty(false)
    private set


  private val moviePageStateFlow: MutableStateFlow<Int> = MutableStateFlow(1)

  private val movieListFlow = moviePageStateFlow.flatMapLatest {
    isMovieListLoading = true
    discoverRepository.loadMoviesGenre(it,ApiConstanst.GENRES) {
      isMovieListLoading = false
    }
  }

  @get:Bindable
  val movieList: List<Movie> by movieListFlow.asBindingProperty(viewModelScope, emptyList())

  private val genreStateFlow: MutableStateFlow<Int> = MutableStateFlow(1)
  private val genreListFlow = genreStateFlow.flatMapLatest {
    isGenreLoading = true
    genreRepository.loadGenres(it){
      isGenreLoading = false
    }
  }

  @get:Bindable
  val genreList: List<Genre> by genreListFlow.asBindingProperty(viewModelScope,emptyList())


  init {
    Timber.d("injection MainActivityViewModel")
  }

  fun postMoviePage(page: Int) = moviePageStateFlow.tryEmit(page)

}
