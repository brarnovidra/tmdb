package id.the.movie.activity.binding

import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.florent37.glidepalette.BitmapPalette
import com.github.florent37.glidepalette.GlidePalette

import id.the.movie.extension.requestGlideListener
import id.the.movie.extension.visible
import id.the.movie.models.entity.Movie
import com.skydoves.whatif.whatIfNotNull
import com.skydoves.whatif.whatIfNotNullOrEmpty
import id.the.movie.models.entity.Genre
import id.the.movie.utils.api.ApiConstanst
import id.the.movie.utils.appendZeroBeforeNumber
import java.text.SimpleDateFormat
import java.util.*


object ViewBinding {

  @JvmStatic
  @BindingAdapter("loadCircleImage")
  fun bindLoadCircleImage(view: AppCompatImageView, url: String) {
    Glide.with(view.context)
      .load(url)
      .apply(RequestOptions().circleCrop())
      .into(view)
  }

  @JvmStatic
  @BindingAdapter("loadPaletteImage", "loadPaletteTarget")
  fun bindLoadImage(view: AppCompatImageView, url: String, targetView: View) {
    Glide.with(view)
      .load(url)
      .listener(
        GlidePalette.with(url)
          .use(BitmapPalette.Profile.VIBRANT)
          .intoBackground(targetView)
          .crossfade(true)
      )
      .into(view)
  }

  @JvmStatic
  @BindingAdapter("visibilityByResource")
  fun bindVisibilityByResource(view: View, anyList: List<Any>?) {
    anyList.whatIfNotNullOrEmpty {
      view.visible()
    }
  }

  @JvmStatic
  @SuppressLint("SetTextI18n")
  @BindingAdapter("bindReleaseDate")
  fun bindReleaseDate(view: TextView, movie: Movie) {
    view.text = "${movie.release_date}"
  }


  @JvmStatic
  @BindingAdapter("bindBackDrop")
  fun bindBackDrop(view: ImageView, movie: Movie) {
    movie.backdrop_path.whatIfNotNull(
      whatIf = {
        Glide.with(view.context).load(ApiConstanst.getBackdropPath(it))
          .listener(view.requestGlideListener())
          .into(view)
      },
      whatIfNot = {
        Glide.with(view.context).load(ApiConstanst.getBackdropPath(movie.poster_path))
          .listener(view.requestGlideListener())
          .into(view)
      }
    )
  }

  @JvmStatic
  @SuppressLint("SetTextI18n")
  @BindingAdapter("bind_genres_text")
  fun TextView.bindGenresText(genres: List<Genre>?) {
    if (genres == null) return

    val maxNumOfGenres = 3
    var text = ""
    val appendText = " / "

    val loopCount = if (genres.size <= maxNumOfGenres) genres.size else maxNumOfGenres
    for (i in 0 until loopCount) {
      text = text + genres[i].name + appendText
    }
    this.text = text.dropLast(appendText.length)
  }

  @JvmStatic
  @SuppressLint("SetTextI18n")
  @BindingAdapter("bind_runtime_text")
  fun bindMovieRuntime(view: TextView,runtimeInMinutes: Int?) {
    runtimeInMinutes?.let {
      val hoursText: String = appendZeroBeforeNumber((it / 60f).toInt())
      val minutesText: String = appendZeroBeforeNumber((it % 60f).toInt())
      val text = "$hoursText:$minutesText / $runtimeInMinutes min"
      view.text = text
    }
  }

  fun getRuntimeDateFormat() = ("yyyy-MM-dd")

  @SuppressLint("SimpleDateFormat")
  @BindingAdapter("bind_date_text")
  fun bindMovieRuntime(view: TextView,dateString: String?) {
    if (dateString.isNullOrBlank()) return
    val date = SimpleDateFormat(getRuntimeDateFormat()).parse(dateString)
    val pat =
      SimpleDateFormat().toLocalizedPattern().replace("\\W?[HhKkmsSzZXa]+\\W?".toRegex(), "")
    val localFormatter = SimpleDateFormat(pat, Locale.getDefault())
    view.text = date?.let { localFormatter.format(it) }

  }

  @JvmStatic
  @SuppressLint("SetTextI18n")
  @BindingAdapter("bind_language_code_text")
  fun TextView.bindMovieLanguage(languageCode: String?) {
    languageCode?.let { this.text = Locale(languageCode).getDisplayLanguage(Locale("en")) }
  }


}
