package id.the.movie.activity.view.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.skydoves.bindables.BindingFragment
import dagger.hilt.android.AndroidEntryPoint
import id.the.movie.R
import id.the.movie.activity.view.adapter.GenreListAdapter
import id.the.movie.activity.view.adapter.MovieListAdapter
import id.the.movie.databinding.MainFragmentMovieBinding

@AndroidEntryPoint
class MovieListFragment :
  BindingFragment<MainFragmentMovieBinding>(R.layout.main_fragment_movie) {

  private val vm: MainActivityViewModel by viewModels()

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    super.onCreateView(inflater, container, savedInstanceState)
    return binding {
      adapter = MovieListAdapter()
      genreListAdapter = GenreListAdapter()
      viewModel = vm
    }.root
  }
}
