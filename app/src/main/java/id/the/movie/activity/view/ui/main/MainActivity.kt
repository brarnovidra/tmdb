package id.the.movie.activity.view.ui.main

import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.skydoves.bindables.BindingActivity
import dagger.hilt.android.AndroidEntryPoint

import id.the.movie.R
import id.the.movie.databinding.ActivityMainBinding
import id.the.movie.extension.applyOnPageSelected

@AndroidEntryPoint
class MainActivity : BindingActivity<ActivityMainBinding>(R.layout.activity_main) {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    initializeUI()
  }

  private fun initializeUI() {
    binding.mainViewpager.setUserInputEnabled(false);
    with(binding.mainViewpager) {
      adapter = MainPagerAdapter(supportFragmentManager, lifecycle)
      offscreenPageLimit = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT
      applyOnPageSelected { binding.mainBottomNavigation.menu.getItem(it).isChecked = true }
      binding.mainBottomNavigation.setOnNavigationItemSelectedListener {
        when (it.itemId) {
          R.id.action_one -> currentItem = 0
        }
        true
      }
    }
  }
}
