package id.the.movie.activity.view.adapter

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.bindables.binding
import id.the.movie.R
import id.the.movie.activity.view.ui.details.movie.MovieDetailActivity
import id.the.movie.databinding.ItemPosterBinding
import id.the.movie.models.entity.Movie
import id.the.movie.utils.api.ApiConstanst


class MovieListAdapter : RecyclerView.Adapter<MovieListAdapter.MovieListViewHolder>() {

  private val itemsMovie: MutableList<Movie> = arrayListOf()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
    val binding = parent.binding<ItemPosterBinding>(R.layout.item_poster)
    return MovieListViewHolder(binding).apply {
      binding.root.setOnClickListener {
        val movie = adapterPosition.takeIf { it != RecyclerView.NO_POSITION }
          ?: return@setOnClickListener
        MovieDetailActivity.startActivityModel(it.context, itemsMovie[movie])
      }
    }
  }

  override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
    with(holder.binding) {
      movie = itemsMovie[position]
      executePendingBindings()
    }
  }

  @SuppressLint("NotifyDataSetChanged")
  fun addMovieList(movies: List<Movie>) {
    if(ApiConstanst.itemsMovie){
      itemsMovie.clear()
      ApiConstanst.itemsMovie = false
      itemsMovie.addAll(movies)
      notifyDataSetChanged()
    }else{
      val previousItemSize = itemsMovie.size
      itemsMovie.addAll(movies)
      notifyItemRangeInserted(previousItemSize, movies.size)
    }
  }

  override fun getItemCount(): Int = itemsMovie.size

  class MovieListViewHolder(val binding: ItemPosterBinding) : RecyclerView.ViewHolder(binding.root)
}
